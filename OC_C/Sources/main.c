//
//  main.c
//  OC_C
//
//  Created by Mathieu Neveu on 15/09/2020.
//  Copyright © 2020 Mathieu Neveu. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
//CONF
const int IS_DEV = 1,MAX=100,MIN=1;
//FUNCTIONS
int initGame(void);
int try(int goodAnswer, int devMod);
int boolUserAnswerListener(void);
char correcting(int suggestion, int goodAnswer);
void gameMastering(char correctingStatus, int goodAnswer);
void gameLaunch(void);
void gameOver(void);
int quit(void);
    //DEV Functions
void devPromptInt(char sentence[100], int var, int position, int carriageReturn);
void devPromptChar(char sentence[100], char var, int position, int carriageReturn);
void devPromptString(char sentence[100], char var[100], int position, int carriageReturn);

int main(int argc, const char * argv[]) {
    
    srand(time(NULL));
    gameLaunch();
    
    //    5) End of the game - gameOver()
    
    return 0;
}

// Generate the random int & display the new game interface
int initGame(){
    int mysterInt = (rand() % (MAX-MIN+1))+MIN;
    printf("=== Jeux du + ou - ===\nTrouvez un nombre tiré au sort entre 1 et 100 !\nLa partie a commencée ! \n");
    return mysterInt;
}


//CORRECTING()
// Manage the UA & display the try interface
// Return values :
// - "L" -> The good answer is lower
// - "H" -> The good answer is higher
// - "G" -> Good answer
// - "S" -> User is surrendering
char correcting(int suggestion, int goodAnswer){
    char correctingStatus='E';
    if (suggestion==666) {
        correctingStatus='S';
    }
    else if (goodAnswer<suggestion){
        correctingStatus='L';
    }
    else if (goodAnswer>suggestion){
        correctingStatus='H';
    }
    else if (goodAnswer==suggestion){
        correctingStatus='G';
    }
    return correctingStatus;
}


//TRY()
//Return an allowed UA [int] (or recall itself in case of critical bad UA)
int try(int goodAnswer, int devMod){
    // if userAnswer[3] it cause a strange error with devMod. If the user type 3 chars, devMod turn to 0 even if it was 1 inside try() just before.
    // Dont happen when char userAnswer[4] so it's surely a neighbourhood memory incident
    char userAnswer[4]={'\0','\0','\0','\0'}, returnValue = 'c';
    const char C_FIGURES[10] = {'0','1','2','3','4','5','6','7','8','9'};
    const int I_FIGURES[10] = {0,1,2,3,4,5,6,7,8,9};
    int userAnswerIsNan = 1, iAllowedUserAnswer=0, numeration=1, intFigureUserAnswer = 0, userAnswerAsInt=0, isCharFigure=0, perfectAllowedUAGlobal=1, easyToFixAllowedUAGlobal=0, isCharVoid=0;
    char allowedUserAnswer[3]={'\0','\0','\0'};
    
    printf("Taper un chiffre entre 1 et 100: ");
    scanf("%s", &userAnswer);
    
    
    //Error Managing, intern step of try()
        //Analyze the userAnswer, 3 ways :
        //  1. Critical error  : "Réponse non acceptée, veuillez réessayer. " relaunch try()
        //  2. Easy to fix error : "Vouliez vous répondre xx ? OUI - non" Suggest an allowed answer if there are some integer in it.
        //  3. No unwanted char : continue with [userAnswerAsInt] without saying anything
        //    --- What's happening ?
        //    Compare UA to [C_FIGURES] to ignore unwanted typed char
        //    if it matches : add this value to the result [userAnswerAsInt]
        //    (& add it to [allowedUserAnswer] char array in {DEVMOD})
    
    //  START OF ErroManaging's FOR
    //  Starting by the end of the array
    for (int iUserAnswer = 2; iUserAnswer>=0; iUserAnswer--) {
        //  If the current char is void, go next char array analys
        if (userAnswer[iUserAnswer]=='\0') {
            userAnswerIsNan=1;
            isCharVoid=1;
        }else{
//      Read it as an integer :
            // Is this char a figure ? If not, the userAnswer char default status is NaN=1 and ignored for the next steps
            for (int iFigures = 0; iFigures<10; iFigures++) {
                if (userAnswer[iUserAnswer]==C_FIGURES[iFigures]) {
                    //Yes, parse it as an int to do maths with
                    userAnswerIsNan = 0;
                    isCharFigure=1;
                    intFigureUserAnswer = I_FIGURES[iFigures];
                    //numeration define if the actual char is a unity, a ten or an hundred
                    //then make the good maths to get the final userAnswer as an integer
                    userAnswerAsInt += numeration * intFigureUserAnswer;
                    //go to next numeration level for the next char
                    numeration *= 10;
                }
            }
        }
        // If this char is a figure or nothing, UA status stay perfect
        if (isCharFigure == 1 || isCharVoid == 1) {
            easyToFixAllowedUAGlobal=1;
        }else{
            perfectAllowedUAGlobal=0;
        }
        isCharFigure= isCharVoid = 0;
//        {DEVMOD}
//   Say if the actual is an allowed answer & Fill [allowedUserAnswer] char array
        if (devMod) {
            devPromptInt("userAnswer's value n°",iUserAnswer+1,1,0);
            printf(" '%c'", userAnswer[iUserAnswer]);
            if (userAnswerIsNan) {
                printf(" is not an allowed answer\n");
            }else{
                printf(" is an allowed answer\n");
                allowedUserAnswer[iAllowedUserAnswer]=userAnswer[iUserAnswer];
                iAllowedUserAnswer++;
            }
        }
        userAnswerIsNan = 1;
    }//END OF ErroManaging's FOR
    
    // If no error, go through the final return
    if (perfectAllowedUAGlobal) {
//        printf("Réponse parfaite, rien à redire !\n");
    }
    // If not a single integer has been typed, reload TRY()
    else if(easyToFixAllowedUAGlobal == 0 && perfectAllowedUAGlobal == 0){
        printf("/!\\ Veuillez répondre uniquement avec des chiffres. /!\\\n");
        userAnswerAsInt = try(goodAnswer, devMod);
    }
    // If UA contains some integer, ask the user to confirm his answer
    else if(easyToFixAllowedUAGlobal){
        printf("Vouliez vous répondre %d ? OUI - non\n", userAnswerAsInt);
        int boolUserAnswerListenerReturn = boolUserAnswerListener();
        //User don't confirm his answer
        if(boolUserAnswerListenerReturn == 0){
            printf("Très bien, nouvelle tentative..\n");
            userAnswerAsInt = try(goodAnswer, devMod);
        }
        //User don't answer properly by yes or no -> abort this question ans TRY() again.
        else if (boolUserAnswerListenerReturn == -1){
            printf("Merci de bien répondre par oui (O) ou par non (n)..\n");
            userAnswerAsInt = try(goodAnswer, devMod);
        }
        //If User confirm his answer : continue
    }
    //        {DEVMOD}
    //   Show [allowedUserAnswer] char array
    if (devMod) {
        //printf("DEV --->  You typed %s\n", userAnswer);
        devPromptString("You typed : ", userAnswer, 1, 1);
    }
    //Game's rules check
    if ((userAnswerAsInt>0 && userAnswerAsInt<100) || userAnswerAsInt==666) {
        //Final return
        return userAnswerAsInt;
    }else{
        printf("Réponse non acceptée, veuillez donner un nombre entre 1 & 100 !\n");
        userAnswerAsInt = try(goodAnswer,IS_DEV);
    }
    return userAnswerAsInt;
}


//GAMEMASTERING()
//Manage correcting() return and comunicate with the player

//      4a) If [goodAnswer] is lower -> Say "C'est moins !" & try()
//      4b) If [goodAnswer] is higher -> Say "C'est plus !"  & try()
//      4c) If [goodAnswer] is equal -> Say "Bravo, vous avez trouvé le nombre mystère !!!" & gameOver()
//      4d) If [surrender] is 1 -> Say "Vous avez abandonné.." & gameOver()

void gameMastering(char correctingStatus, int goodAnswer){
    switch (correctingStatus) {
        case 'E':
            printf("Une erreur est survenue lors de la correction, veuillez réessayer.\n");
            break;
        case 'L':
            printf("C'est moins !\n");
            break;
        case 'H':
            printf("C'est plus !\n");
            break;
        case 'G':
            printf("Bravo, vous avez trouvé le nombre mystère !!!\n==== GAME OVER ====\n");
            gameOver();
            break;
        case 'S':
            printf("Vous avez abandonné..\n==== GAME OVER ====\n");
            gameOver();
        default:
            break;
    }
}
// GAMELAUNCH()
// Launch a game
void gameLaunch(){
    int goodAnswer=0,userAnswer=0,tryCount=-1;
    char correctingStatus=0;
    //    1) Random 1-100 - initGame()
    goodAnswer = initGame();
    do {
    //    2) Get user's answer (UA) - try()
        userAnswer = try(goodAnswer,IS_DEV);
        
    //    3) Analysing UA - correcting()
        correctingStatus = correcting(userAnswer, goodAnswer);
        
    //    4) Game mastering - gameMastering()
        gameMastering(correctingStatus,goodAnswer);
        if (tryCount%4==0 && tryCount>10) {
            printf("Si vous souhaitez abandonner cette partie, tapez 666 !\n");
        }
        tryCount++;
    } while (userAnswer!=goodAnswer);
}

// GAMEOVER()
// Prompt a game over visual
// Ask "Voulez-vous commencer une nouvelle partie ?"
// Depend on what is answered : Yes -> initGame(), No -> quit()
void gameOver(){
    printf("Voulez-vous commencer une nouvelle partie ? OUI - non\n");
    int boolUserAnswerListenerReturn = boolUserAnswerListener();
    if (boolUserAnswerListenerReturn==0) {
        printf("Très bien, vous quittez le jeu.\nÀ très vite !\n");
        quit();
    }
    else if (boolUserAnswerListenerReturn==-1){
        printf("Merci de bien répondre par oui (O) ou par non (n)..\n");
        gameOver();
    }else{
        printf("Très bien ! Lancement d'une nouvelle partie..\n\n");
        gameLaunch();
    }
}

// QUIT()
// End of the program
int quit(){
    exit(0);
}

// BOOLUSERANSWERLISTENER()
// return a translation of a user 'yes or not' answer by a boolean value (or -1 error)
// So the user can answer in English or French :
// -> 1 : y / Y / Yes / YES / yes / O / o / oui / OUI / carriage return
// -> 0 : n / N / No / NO / no / Non / NON
// -> -1 : other
int boolUserAnswerListener(){
    char boolUserAnswer[3] = {'\0','\0','\0'};
    scanf(" %s", boolUserAnswer);
    int boolStatus=-1;
    if (boolUserAnswer[0] == 'o' || boolUserAnswer[0] == 'O' || boolUserAnswer[0] == 'y' || boolUserAnswer[0] == 'Y') {
        return boolStatus = 1;
    }else if (boolUserAnswer[0] == 'n' || boolUserAnswer[0] == 'N'){
        return boolStatus = 0;
    }else
        return boolStatus;
}
