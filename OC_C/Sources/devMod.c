//
//  devMod.c
//  OC_C
//
//  Created by Tatadmound on 18/01/2021.
//  Copyright © 2021 Mathieu Neveu. All rights reserved.
//

#include "../Headers/devMod.h"

//  {DEVMOD}
//  Use the clearest way to prompt devMod integer data
//  [sentence] to describe the data
//  [var] the data variable
//  [position] 0 -> before the sentence, 1 -> after
//  [carriageReturn] 1 -> do a carriage return, 0 -> don't
void devPromptInt(char sentence[100], int var, int position, int carriageReturn){
    printf("DEV --->  ");
    if (position==0) {
        printf("%d",var);
        printf("%s",sentence);
    }else{
        printf("%s",sentence);
        printf("%d",var);
    }
    if (carriageReturn) {
        printf("\n");
    }
}

//  {DEVMOD}
//  Use the clearest way to prompt devMod char data
//  [sentence] to describe the data
//  [var] the data variable
//  [position] 0 -> before the sentence, 1 -> after
//  [carriageReturn] 1 -> do a carriage return, 0 -> don't
void devPromptChar(char sentence[100], char var, int position, int carriageReturn){
    printf("DEV --->  ");
    if (position==0) {
        printf("%c",var);
        printf("%s",sentence);
    }else{
        printf("%s",sentence);
        printf("%c",var);
    }
    if (carriageReturn) {
        printf("\n");
    }
}

//  {DEVMOD}
//  Use the clearest way to prompt devMod string data
//  [sentence] to describe the data
//  [var] the data variable /!\ maxSize = 100 /!\
//  [position] 0 -> before the sentence, 1 -> after
//  [carriageReturn] 1 -> do a carriage return, 0 -> don't
void devPromptString(char sentence[100], char var[100], int position, int carriageReturn){
    printf("DEV --->  ");
    if (position==0) {
        printf("%s",var);
        printf("%s",sentence);
    }else{
        printf("%s",sentence);
        printf("%s",var);
    }
    if (carriageReturn) {
        printf("\n");
    }
}
